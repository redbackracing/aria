# Aria

Aria is a visualisation platform for telemetry data from the Redback Racing car.

## Installation for Developers

1. Aria requires Node and its package manager, NPM, to be installed. Windows and Mac users can download the installer [here](https://nodejs.org/en/download/), while Linux users should install Node using their standard package manager.
2. Install global packages on the command line: `npm install -g gulp gulp-cli http-server`
3. Clone the Aria git repository and navigate to the `app` directory.
4. Run `npm install`, followed by `npm install --dev`, to install all of Aria's dependencies.

### Commands

*   `gulp build`: Build a new version of the website in the `app/dist` folder.
*   `gulp test`: Build a testing version of the website in the `app/dist` folder and launch a Jasmine test server. (**Note:** to use the regular version, just run `gulp build` again. Jasmine requires modified dependencies)
*   `gulp lint`: Run ESLint on all source files and report errors.
*   `npm run serve`: Runs a development server that serves the built website from `app/dist`.

## Distribution

Run `gulp dist` to populate the `app/dist` folder with a user release. Only this folder needs to be shared with users, and it contains Windows and Linux server scripts to ensure it runs across platforms.

## Summary

1. Run a development server with `npm run serve`.
2. Edit code, following the styleguide and using ES6 features.
3. Build the site with `gulp build` and refresh the browser to see changes.
