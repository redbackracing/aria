//useful documentation
//https://docs.omniref.com/js/npm/forerunnerdb/1.0.24
describe("$fdb service test", function () {

    beforeEach(angular.mock.module('DashboardApp'));

    describe("Testing database calling", function () {
        var db;
        beforeEach(function () {
            window.fdb = new ForerunnerDB();
        });
        it('Checks that fdb creates new database', function () {
            //creating database object called db
            expect(db).not.toBeDefined();
            db = fdb.db('testDB');
            expect(db).not.toEqual(null);
        });
    });

    describe("Testing database storage", function () {
        // create a database and collection before each test
        beforeEach(function () {
            window.fdb = new ForerunnerDB();
            this.db = fdb.db('testDB');
            this.db.collection('testItems');
        })
        it('Checks that .insert inserts entry and .find returns as an array of items', function () {
            //adding "new item" to forerunner
            var item = "new item";
            this.db.collection('testItems').insert({
                text: "item"
            });

            //telling forerunner to persist data
            this.db.collection('testItems').save();

            //creating array to call stored item
            // find returns an array with all matching documents of search
            var array = this.db.collection('testItems').find({
                text: "item"
            });
            // check insert function
            expect(array[0].text).toEqual("item");
        });
    });

    describe("Testing updating data", function () {
        beforeEach(function () {
            window.fdb = new ForerunnerDB();
            this.db = fdb.db('testDB');
            this.db.collection('testItems');
        })
        it('Checks that .update replaces and updates current stored data', function () {
            this.db.collection('testItems').insert({
                text: "item"
            });
            // updating data with new fields and values
            this.db.collection('testItems').update({
                text: "item"
            }, {
                    $replace: {
                        text_new: "item_new",
                        text_new2: "item_new2"

                    }
                });
            // checking document before update
            array = this.db.collection('testItems').find();
            // checking updated data
            expect(array[0].text_new).toEqual("item_new");
            expect(array[0].text_new2).toEqual("item_new2");
        });
    });

    describe("Testing pushing/popping data from db", function () {
        beforeEach(function () {
            window.fdb = new ForerunnerDB();
            this.db = fdb.db('testDB');
            this.db.collection('testItems');
        })

        it('Checks that $push operator appends a value to the array', function () {
            this.db.collection("testItems").insert({
                _id: "test1",
                testList: []
            });
            array = this.db.collection('testItems').find();
            expect(array[0].testList).toEqual([]);
            this.db.collection("testItems").update({
                _id: "test1"
            }, {
                    $push: {
                        testList: "Channel Number"
                    }
                });

            array = this.db.collection('testItems').find();
            expect(array[0].testList).toEqual(["Channel Number"]);

        });

        it('Checks that $splicePush operator adds item to specified index of array', function () {
            this.db.collection("testItems").insert({
                _id: "test1",
                testList: ["Channel Number"]
            });
            array = this.db.collection('testItems').find();
            expect(array[0].testList[0]).toEqual("Channel Number");
            this.db.collection("testItems").update({
                _id: "test1"
            }, {
                    $splicePush: {
                        testList: "t1",
                        $index: 0
                    }
                });

            array = this.db.collection('testItems').find();
            expect(array[0].testList[0]).toEqual("t1");

            this.db.collection("testItems").update({
                _id: "test1"
            }, {
                    $splicePush: {
                        testList: "t2",
                        $index: 2
                    }
                });
            array = this.db.collection('testItems').find();
            expect(array[0].testList[2]).toEqual("t2");

        });

        it('Checks that $splicePull operator removes item to specified index of array', function () {
            this.db.collection("testItems").insert({
                _id: "test1",
                testList: ["t1"]
            });

            this.db.collection("testItems").update({
                _id: "test1"
            }, {
                    $push: {
                        testList: "Channel Number"
                    }
                });
            array = this.db.collection('testItems').find();
            expect(array[0].testList[0]).toEqual("t1");
            expect(array[0].testList[1]).toEqual("Channel Number");

            this.db.collection("testItems").update({
                _id: "test1"
            }, {
                    $splicePull: {
                        testList: {
                            $index: 0
                        }
                    }
                });

            array = this.db.collection('testItems').find();
            expect(array[0].testList[0]).toEqual("Channel Number");

            this.db.collection("testItems").update({
                _id: "test1"
            }, {
                    $splicePull: {
                        testList: {
                            $index: 1
                        }
                    }
                });
            array = this.db.collection('testItems').find();
            expect(array[0].testList[0]).toEqual("Channel Number");

        });

        it('Checks that $pull operator removes a value or values that match an input query', function () {
            this.db.collection("testItems").insert({
                _id: "test1",
                testList: ["Channel Number"]
            });

            this.db.collection("testItems").update({
                _id: "test1"
            }, {
                    $push: {
                        testList: "t1"
                    }
                });
            array = this.db.collection('testItems').find();
            expect(array[0].testList[0]).toEqual("Channel Number");
            expect(array[0].testList[1]).toEqual("t1");
            expect(array[0].testList.length).toEqual(2);

            this.db.collection("testItems").update({
                _id: "test1"
            }, {
                    $pull: {
                        testList:
                        "Channel Number",
                    }
                });
            array = this.db.collection('testItems').find();
            expect(array[0].testList).toEqual(["t1"]);

        });

        it('Checks that pop removes an element from the end/begining of array if you pass 1/-1 respectively ', function () {
            this.db.collection("testItems").insert({
                _id: "test2",
                testList: [{
                    name: "One"
                }, {
                    name: "Two"
                }, {
                    name: "Three"
                }, {
                    name: "Four"
                }]
            });
            array = this.db.collection('testItems').find();
            expect(array[0].testList[0].name).toEqual("One");
            expect(array[0].testList[1].name).toEqual("Two");
            expect(array[0].testList[2].name).toEqual("Three");
            expect(array[0].testList[3].name).toEqual("Four");
            expect(array[0].testList.length).toEqual(4);

            this.db.collection("testItems").update({
                _id: "test2"
            }, {
                    $pop: {
                        testList: -1 // -1 pops from the beginning, 1 pops from the end
                    }
                });
            array = this.db.collection('testItems').find();
            expect(array[0].testList[0].name).toEqual("Two");
            expect(array[0].testList[1].name).toEqual("Three");
            expect(array[0].testList[2].name).toEqual("Four");
            expect(array[0].testList.length).toEqual(3);

            this.db.collection("testItems").update({
                _id: "test2"
            }, {
                    $pop: {
                        testList: 1 // -1 pops from the beginning, 1 pops from the end
                    }
                });

            array = this.db.collection('testItems').find();
            expect(array[0].testList[0].name).toEqual("Two");
            expect(array[0].testList[1].name).toEqual("Three");
            expect(array[0].testList.length).toEqual(2);
        });

    });

    describe("Testing removing documents", function () {
        beforeEach(function () {
            window.fdb = new ForerunnerDB();
            this.db = fdb.db('testDB');
            this.db.collection('testItems');
        })

        it('Checks .drop removes instaces of memory e.g. database/collections/views documents of search', function () {

            // Insert a record
            this.db.collection('testItems').insert({
                _id: 1,
                name: 'Test'
            });
            // before drop db
            array = this.db.collection('testItems').find();
            expect(array[0]._id).toEqual(1);
            expect(array[0].name).toEqual("Test");

            // drop entire database
            this.db.drop();
            this.db = fdb.db('testDB');
            array = this.db.collection('testItems').find();
            console.log('After drop', this.db.collections('testItems'));
        });

    });

});
