describe("Data Service", function () {
    var data =
        [

            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 1, "value": 3.249000072479248, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 2, "value": 3.2609999179840088, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 3, "value": 3.2609999179840088, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 4, "value": 3.250999927520752, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 5, "value": 3.25, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 6, "value": 3.2609999179840088, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 7, "value": 3.2609999179840088, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 8, "value": 3.255000114440918, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 9, "value": 3.247999906539917, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 10, "value": 3.253000020980835, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 11, "value": 3.2599999904632568, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 12, "value": 3.2569999694824219, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 13, "value": 3.2420001029968262, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 14, "value": 3.244999885559082, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 15, "value": 3.2599999904632568, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 16, "value": 3.250999927520752, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 17, "value": 3.2539999485015869, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 18, "value": 3.247999906539917, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 19, "value": 3.25, "warn": true },
            { "channelname": "Test Temperature Sensor", "channelnum": 1, "diag": true, "time": 20, "value": 3.2599999904632568, "warn": true }

        ];

    var DataService;

    beforeEach(angular.mock.module('DashboardApp'));

    beforeEach(inject(function (_DataService_) {
        DataService = _DataService_;
        DataService.importData(data);
    }));

    afterEach(inject(function () {
        DataService.clearData();
    }));

    it(" should be able to convert a name to a channel number", function () {
        expect(DataService.convertNameToNumber("Test Temperature Sensor")).toEqual(1);
    });

    it(" should be able to return an array of values for a given channel", function () {
        expect(DataService.convertNameToNumber("Test Temperature Sensor")).toEqual(0);
    });

    it(" should be able to return an array of times for a given channel", function () {
        expect(DataService.convertNameToNumber("Test Temperature Sensor")).toEqual(0);
    });

    it(" should be able to return a unique channel name for given channel", function () {
        expect(DataService.convertNameToNumber("Test Temperature Sensor")).toEqual(0);
    });

    it(" should be able to return an array of booleans for diagnostic and warning flagged samples", function () {
        expect(DataService.convertNameToNumber("Test Temperature Sensor")).toEqual(0);
    });

    it(" should be able to return an array of channels in the form Dashboard Controller likes it", function () {
        expect(DataService.convertNameToNumber("Test Temperature Sensor")).toEqual(0);
    });

    it(" should be able to return an object which will be able to link to High Charts", function () {
        expect(DataService.convertNameToNumber("Test Temperature Sensor")).toEqual(0);
    });


});