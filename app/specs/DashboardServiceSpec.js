describe("Dashboard Service:", function () {
    var DashboardService;
    console.log(angular);
    beforeEach(angular.mock.module("DashboardApp"));
    beforeEach(inject(function (_DashboardService_) {
        DashboardService = _DashboardService_;
    }));

    describe("Existence of", function () {
        it("DashboardService", function () {
            expect(DashboardService).toBeDefined();
        });
        it("DashboardService.getCurrent", function () {
            expect(DashboardService.getCurrent).toBeDefined();
        });
        it("DashboardService.setCurrent", function () {
            expect(DashboardService.setCurrent).toBeDefined();
        });
    });

    describe("Initial value of ", function () {
        it("current dashboard should initially be null", function () {
            expect(DashboardService.getCurrent()).toEqual(null);
        });
    });

    describe("Setting current dashboard value to", function () {
        it("test.dash", function () {
            DashboardService.setCurrent("test.dash");
            expect(DashboardService.getCurrent()).toEqual("test.dash");
        });
        it("test 1.dash", function () {
            DashboardService.setCurrent("test 1.dash");
            expect(DashboardService.getCurrent()).toEqual("test 1.dash");
        });
        it("test2.dash", function () {
            DashboardService.setCurrent("test2.dash");
            expect(DashboardService.getCurrent()).toEqual("test2.dash");
        });
        it("test-3.dash", function () {
            DashboardService.setCurrent("test-3.dash");
            expect(DashboardService.getCurrent()).toEqual("test-3.dash");
        });
        it("test_4.dash", function () {
            DashboardService.setCurrent("test_4.dash");
            expect(DashboardService.getCurrent()).toEqual("test_4.dash");
        });
        it("TEST #5.dash", function () {
            DashboardService.setCurrent("TEST #5.dash");
            expect(DashboardService.getCurrent()).toEqual("TEST #5.dash");
        });
    });
});
