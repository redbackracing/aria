var angular = require('angular');

var app = angular.module('DashboardApp');

var Highcharts = require('highcharts');
window.Highcharts = Highcharts;
require('highcharts/modules/exporting')(Highcharts);

app.directive("highchartsGraph", function () {
    return {
        scope: {
            options: '=',
        },
        link: function (scope, element, attrs) {
            Highcharts.chart(element[0], scope.options);
        }
    };
});

app.directive('dashboardDir', function () {
    return {
        templateUrl: '/directives/dashboardChart.html'
    };

});

app.directive('drawDir', function () {
    return function (scope, element, attrs) {
        console.log("Draw")
    };
})

app.directive('fileReader', function () {
    return {
        scope: {
            fileReader: "="
        },
        link: function (scope, element) {
            $(element).on('change', function (changeEvent) {
                var files = changeEvent.target.files;
                if (files.length) {
                    var r = new FileReader();
                    r.onload = function (e) {
                        var contents = e.target.result;
                        scope.$apply(function () {
                            scope.fileReader = contents;
                        });
                    };

                    //scope.$emit('File Read Start');

                    //Reads a single file
                    r.readAsText(files[0]);

                    //scope.$emit('File Read End');
                }
            });
        }
    };
});

app.directive('ngConfirmClick', [
    function(){
        return {
            link: function (scope, element, attr) {
                var msg = scope.dialog;
                var clickAction = attr.confirmedClick;
                element.bind('click',function (event) {
                    msg = scope.dialog;
                    if ( window.confirm(msg) ) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
}])
