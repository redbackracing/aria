// Highcharts requires JQuery to be in the global scope so some import magic is
// needed.
var jquery = require('jquery');
global.$ = global.jQuery = jquery;

require('bootstrap');

var angular = require('angular');
require('angular-ui-router');
require('angular-websocket');
require('angular-widget-grid');
require('angularjs-dropdown-multiselect');
const ngMock = require('angular-mocks/ngMock');

require('../dist/templateCachePartials');
require('../dist/templateCacheDirectives');

var app = angular.module('DashboardApp', ['ui.router', 'angular-websocket', 'widgetGrid', 'angularjs-dropdown-multiselect', 'ariaPartials', 'ariaDirectives']);

app.config(function ($stateProvider, $urlRouterProvider) {
    //Provide a default route, just in case for unmatched urls
    $urlRouterProvider.otherwise("/DashboardSelection");

    //Set up routes so that you can traverse between pages easily.
    $stateProvider
        .state('DashboardSelection', {
            url: "/DashboardSelection",
            templateUrl: "/partials/DashboardSelection.html",
        })
        .state('Dashboard', {
            url: "/Dashboard",
            templateUrl: "/partials/Dashboard.html",
        })
        .state('ConfigEditor', {
            url: "/ConfigEditor",
            templateUrl: "/partials/ConfigEditor.html",
        })
        .state('WirelessTelemetry', {
            url: "/WirelessTelemetry",
            templateUrl: "/partials/WirelessTelemetry.html",
        });

});

require('DashboardController');
require('dashboardChartCtrl');
require('importCtrl');
require('WirelessController');

require('DashboardService');
require('DataService');

require('directives');