const angular = require('angular');
require('angular-widget-grid');

const app = angular.module('DashboardApp');

const Highcharts = require('highcharts');

window.Highcharts = Highcharts;
require('highcharts/modules/exporting')(Highcharts);

app.controller('dashboardChartCtrl', ($scope, $interval, $timeout, DataService, DashboardService) => {
    $scope.isNotLocked = false;
    $scope.text = 'Unlock Dashboard';
    $scope.layoutText = 'Export Dashboard';
    $scope.dialog = 'Exporting dashboard layout. The current layout will be saved.';
    $scope.showGrid = false;
    $scope.gridRows = 15;

    $scope.data = [];
    $scope.labels = [];
    $scope.type = [];
    $scope.channelNum = [];
    $scope.channelName = [];

    $scope.selectedChannels = [];
    
    var datasetName;

    const data = [];
    let emptyCount = 0;

    var graphCount = 0;     // Let this keep track of the number of graphs added by the user. i.e. the first graph will have index 0.

    const promise = $interval(reflow, 500);
    const promiseRows = $interval(addRows,500);

    //dynamically add grid rows
    function addRows() {
        angular.forEach($scope.charts, function(chart) {
            if (chart.position.height + chart.position.top - 1 == $scope.gridRows) {
                $scope.gridRows += 10;
            }
        })
    }

    $scope.fileContent = "";
    $scope.import3 = function() {
        $scope.uploadEnabled = false;
        console.log("import3");
        var fileData = $scope.fileContent;
        //console.log(fileData);
        fileData = JSON.parse(fileData);
        datasetName = fileData[0].start;
        console.log(datasetName);
        DataService.importDataset(fileData);
        populate(datasetName);
        $scope.uploadEnabled = true;
    }
    
    $scope.uploadEnabled = true;
    $scope.upload_btn = false;
    $scope.uploadText = 'Upload';

    $scope.import = () => {
        let fileData = $scope.fileContent;
        console.log(fileData);

        $scope.uploadText = 'Starting upload';
        console.log($scope.uploadText);

        fileData = fileData.replace(/\n/g, ',');

        fileData = '[' + fileData;

        // Replace last character with ] as it is currently a comma
        fileData = fileData.slice(0, -1) + ']';

        // Clear out previous data set
        DataService.clearData();

        fileData = JSON.parse(fileData);

        // Should perform some error checking
        if (!DataService.importData(fileData)) {
            $scope.uploadText = 'Upload Failed';
            console.log($scope.uploadText);
        } else {
            $scope.uploadText = 'Upload Finished';
            console.log($scope.uploadText);
            $scope.upload_btn = !$scope.upload_btn;
        }

        $scope.uploadEnabled = false;

        // Populates the channel selection dropdown
        populate();
    };
    
    $scope.import2 = function() {
        DataService.importDataset(
            [
                {"start": "2017-08-12-1136"},
                {"channelname":"channel 2","channelnum":2,"diag":true,"time":0,"value":63.875,"warn":true},
                {"channelname":"first channel","channelnum":1,"diag":true,"time":0.5,"value":69.808,"warn":true},
                {"channelname":"Test Temperature Sensor","channelnum":7,"diag":true,"time":1,"value":72.599,"warn":true},
                {"channelname":"first channel","channelnum":1,"diag":true,"time":1.5,"value":75.04,"warn":true},
                {"channelname":"first channel","channelnum":1,"diag":true,"time":2,"value":76.011,"warn":true},
                {"channelname":"channel 2","channelnum":2,"diag":true,"time":2.5,"value":75.933,"warn":true},
                {"channelname":"channel 2","channelnum":2,"diag":true,"time":3.0,"value":67.229,"warn":true}
            ]
        );
        populate();
    }

    $scope.$on('File Read Start', () => {
        $scope.uploadEnabled = false;
        $scope.uploadText = 'Checking file';
    });

    $scope.$on('File Read End', () => {
        $scope.uploadEnabled = true;
        $scope.upload_btn = !$scope.upload_btn;
        $scope.uploadText = 'File ready';
    });

    $scope.graphChannels = []; 
    $scope.graphChannels2D = {};

    function populate(ds) {
        console.log("populate()");
        $scope.channels = DataService.getChannels(ds);                        
        
        $scope.graphChannels = [];
        
        if ($scope.graphChannels.length != 0) {
            $scope.graphChannels = [];
        } 

        for (var j = 0; j < $scope.channels.length; j++) {
            $scope.graphChannels.push({id: $scope.channels[j].channelnum, label: $scope.channels[j].channelname});
            console.log("Channel data has been pushed!");
        }

    };


    $scope.toggle = () => {
        if ($scope.isNotLocked === true) {
            $scope.isNotLocked = false;
            $scope.text = 'Unlock Dashboard';
            $scope.layoutText = 'Export Dashboard';
            $scope.dialog = 'Exporting dashboard layout. The current layout will be saved.';
            $scope.showGrid = false;

            //destroy excess grid rows
            $scope.gridRows = 15;
            if ($scope.charts != "") {
                angular.forEach($scope.charts, function(chart) {
                    if (chart.position.height + chart.position.top > $scope.gridRows) {
                        $scope.gridRows = chart.position.height + chart.position.top;
                    }
                })
            }
        } else {
            $scope.isNotLocked = true;
            $scope.text = 'Lock Dashboard';
            $scope.layoutText = 'Import Dashboard';
            $scope.dialog = 'Importing dashboard layout. The current layout will be lost.';
            $scope.showGrid = true;
        }
    };

    $scope.dashLayout = () => {
        const dashboard = DashboardService.getCurrent();
        if ($scope.isNotLocked === false) {
            //Export Dashboard Layout
            DataService.saveDash(dashboard, $scope.charts, $scope.selectedChannels);
        } else {
            //Import Dashboard Layout
            //Restores $scope.charts
            $scope.gridRows = 15;
            $scope.charts = [];
            emptyCount = 0;
            const dash = DataService.loadDash(dashboard).dash;
            angular.forEach(dash, function(chart) {
                if (chart.position.height + chart.position.top > $scope.gridRows) {
                        $scope.gridRows = chart.position.height + chart.position.top;
                }
                $scope.addChart(chart.type, chart.position.top, chart.position.height, chart.position.left, chart.position.width);
            })
            $scope.uploadEnabled = false;
            populate();

            //Restores $scope.selectedChannels
            $timeout(function(){
                $scope.selectedChannels = DataService.loadDash(dashboard).channel;
                angular.forEach($scope.charts, function(chart) {
                    if ($scope.selectedChannels[chart.id] != '') {
                        $scope.addChannel(chart);
                    }
                })
            },500)
        }
    };

    // Example chart data and adding/removing functions
    $scope.charts = [];
    $scope.chartTypes = ['area', 'bubble', 'bar', 'column', 'gauge', 'line', 'pie', 'scatter', 'synchronised'];

    $scope.addChart = (type, top, height, left, width) => {
        $scope.charts.push({
            name: 'Empty',
            display: true,
            id: emptyCount,
            position: {
                top: top,
                height: height,
                left: left,
                width: width,
            },
            type,
        });

        $scope.selectedChannels[emptyCount] = [];
        emptyCount += 1;
    };

    $scope.addChannel = (chart) => {
        console.log("addchannel fn");
        console.log(chart);
        let i = 0;
        const channelNum = [];
        const channelData = [];

        angular.forEach($scope.selectedChannels[chart.id], (value, key) => {
            console.log("?????????????????????????????????");
            console.log($scope.selectedChannels[chart.id]);
            channelNum[i] = $scope.selectedChannels[chart.id][i].id;
            channelData[i] = DataService.getHighChartsChannelData(datasetName, channelNum[i]);
            console.log("channelNum[i] is " + channelNum[i]);
            console.log("channel data in addchannel");
            console.log(channelData[i]);
            i += 1;
        });

        if (chart.type === 'synchronised') {
            drawSynchronisedChart(chart, channelData, i);
        } else {
            drawChart(chart, channelData, i);
        }
    };

    function drawChart(chart, channelData, n) {
        console.log("draw chart");
        $('#' + chart.id).empty();
        console.log("channeldata in drawchart:");
        console.log(channelData);
        $('#' + chart.id).highcharts({
            chart: {
                type: chart.type,
                zoomType: 'xy',
                panning: true,
                panKey: 'shift',
                borderColor: '#346691',
                borderWidth: 1,
            },
            subtitle: {
                text: 'Click and drag to zoom in. Hold down shift key to pan.'
            },
            xAxis: {
                title: {
                    enabled: true,
                    text: 'Time (s)',
                }
            },
            navigation: {
                buttonOptions: {
                    enabled: true,
                    align: 'left'
                }
            },
            // series: [{
            //     name: 'Installation',
            //     data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
            // }, {
            //     name: 'Manufacturing',
            //     data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
            // }, {
            //     name: 'Sales & Distribution',
            //     data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
            // }, {
            //     name: 'Project Development',
            //     data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227]
            // }, {
            //     name: 'Other',
            //     data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]
            // }],
        });

        const hChart = $('#' + chart.id).highcharts();
        let chartTitle = '';
        for (let i = 0; i < n; i += 1) {
            hChart.addSeries({
                name: channelData[i].channelName,
                data: channelData[i].data,
            });
            chartTitle = chartTitle.concat(channelData[i].channelName);
            if (i !== n - 1) {
                chartTitle = chartTitle.concat(' & ');
            }
        }

        if (channelData[0].diagLow < 1000) {
            hChart.yAxis[0].addPlotLine({
                label: {
                    text: 'Diag Low',
                },
                value: channelData[0].diagLow,
                color: 'orange',
                width: 2,
                zIndex: 5,
            });
        }
        if (channelData[0].warnLow < 1000) {
            hChart.yAxis[0].addPlotLine({
                label: {
                    text: 'Warn Low',
                },
                value: channelData[0].warnLow,
                color: 'red',
                width: 2,
                zIndex: 10,
            });
        }
        if (channelData[0].diagHigh > 0) {
            hChart.yAxis[0].addPlotLine({
                label: {
                    text: 'Diag High',
                },
                value: channelData[0].diagHigh,
                color: 'orange',
                width: 2,
                zIndex: 5,
            });
        }
        if (channelData[0].warnHigh > 0) {
            hChart.yAxis[0].addPlotLine({
                label: {
                    text: 'Warn High',
                },
                value: channelData[0].warnHigh,
                color: 'red',
                width: 2,
                zIndex: 10,
            });
        }

        hChart.title.update({
            text: chartTitle,
        });
    }

    function drawSynchronisedChart(chart, channelData, n) {
        const chHeight = $('#' + chart.id).height() / n;

        $('#' + chart.id).empty();

        $('.chart').height(chHeight);

        $('#' + chart.id).bind('mousemove touchmove touchstart', (e) => {
            Highcharts.charts.forEach((currentChart) => {
                const event = currentChart.pointer.normalize(e.originalEvent);

                if (currentChart.series.length > 0) {
                    // Get the hovered point
                    const point = currentChart.series[0].searchPoint(event, true);

                    if (point) {
                        point.highlight(e);
                    }
                }
            });
        });

        /**
         * Override the reset function, we don't need to hide the tooltips and crosshairs.
         */
        Highcharts.Pointer.prototype.reset = () => {
            return undefined;
        };

        /**
         * Highlight a point by showing tooltip, setting hover state and draw crosshair
         */
        Highcharts.Point.prototype.highlight = (event) => {
            this.onMouseOver(); // Show the hover marker
            this.series.chart.tooltip.refresh(this); // Show the tooltip
            this.series.chart.xAxis[0].drawCrosshair(event, this); // Show the crosshair
        };

        /**
         * Synchronize zooming through the setExtremes event handler.
         */
        function syncExtremes(e) {
            const thisChart = this.chart;

            if (e.trigger !== 'syncExtremes') { // Prevent feedback loop
                Highcharts.each(Highcharts.charts, (currentChart) => {
                    if (currentChart !== thisChart) {
                        if (currentChart.xAxis[0].setExtremes) { // It is null while updating
                            currentChart.xAxis[0].setExtremes(e.min, e.max, undefined, false, {
                                trigger: 'syncExtremes',
                            });
                        }
                    }
                });
            }
        }

        for (let i = 0; i < n; i += 1) {
            $('<div id=' + chart.type + '' + channelData[i].channelNum + '>').appendTo('#' + chart.id).highcharts({
                chart: {
                    marginLeft: 40, // Keep all charts left aligned
                    spacingBottom: 55,
                    height: chHeight,
                    zoomType: 'xy',
                    panning: true,
                    panKey: 'shift',
                    borderColor: '#346691',
                    borderWidth: 2,
                },
                title: {
                    text: channelData[i].channelName,
                    align: 'center',
                    margin: 0,
                    x: 30,
                },
                credits: {
                    enabled: false,
                },
                legend: {
                    enabled: false,
                },
                xAxis: {
                    crosshair: {
                        width: 3,
                        color: 'black',
                    },
                    events: {
                        setExtremes: syncExtremes,
                    },
                    categories: channelData[i].time,
                },
                yAxis: {
                    title: {
                        text: null,
                    },
                },
                exporting: {
                    enabled: false,
                },
                tooltip: {
                    positioner: () => {
                        return {
                            x: this.chart.chartWidth - this.label.width, // right aligned
                            y: -1, // align to title
                        };
                    },
                    borderWidth: 0,
                    backgroundColor: 'none',
                    pointFormat: '{point.y}',
                    headerFormat: '',
                    shadow: false,
                    style: {
                        fontSize: '18px',
                    },
                },
                series: [{
                    data: channelData[i].data,
                    name: channelData[i].channelName,
                    type: 'line',
                    color: Highcharts.getOptions().colors[i],
                    fillOpacity: 0.3,
                }],
            });
        }
    }

    $scope.removeChart = (chart) => {
        const index = $scope.charts.indexOf(chart);
        $scope.charts.splice(index, 1);
    };

    function reflow() {
        angular.forEach($scope.charts, (chartObject) => {
            const chart = $('#' + chartObject.id).highcharts();
            if (chart != null) {
                chart.reflow();
            } else if (chartObject.type === 'synchronised') {
                const childrenChart = [];
                $('div', '#' + chartObject.id).each(() => {
                    childrenChart.push($(this).attr('id'));
                });

                const chHeight = $('#' + chartObject.id).height() / (childrenChart.length / 2);
                const width = $('#' + chartObject.id).width();

                angular.forEach(childrenChart, (value) => {
                    if (value.indexOf('highcharts') === -1) {
                        const childChart = $('#' + value).highcharts();
                        if (childChart != null) {
                            childChart.setSize(width, Math.floor(chHeight), false);
                        }
                    }
                });
            }
        });
    }

    function mockData(channelNum, channelName) {
        const channelData = {};
        channelData.channelNum = channelNum;

        channelData.channelName = channelName;

        channelData.data = [5 * Math.random(), 5 * Math.random(), 5 * Math.random(), 5 * Math.random(), 5 * Math.random(), 5 * Math.random(), 5 * Math.random(), 5 * Math.random(), 5 * Math.random(), 5 * Math.random()];
        channelData.time = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0];

        return channelData;
    }
});
