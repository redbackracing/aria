var angular = require('angular');

var app = angular.module('DashboardApp');

app.controller('ImportController', function ($scope, DataService) {
    $scope.uploadEnabled = false;
    $scope.uploadText = "";

    $scope.import = function () {
        var data = $scope.fileContent;

        $scope.uploadText = "Starting upload";

        data = data.replace(/\n/g, ",");

        data = "[" + data;

        //Replace last character with ] as it is currently a comma
        data = data.slice(0, -1) + "]";

        //Clear out previous data set
        DataService.clearData();

        //Should perform some error checking
        DataService.importData(data);

        $scope.uploadText = "Upload Finished";
        $scope.uploadEnabled = false;
    };

    $scope.$on('File Read Start', function () {
        $scope.uploadEnabled = false;
        $scope.uploadText = "Checking file";
    });

    $scope.$on('File Read End', function () {
        $scope.uploadEnabled = true;
        $scope.uploadText = "File ready";
    });

    
    $scope.insertTest = function() {
        console.log("inserting");
        DataService.importDataset(
            [
                {"start": "2017-08-12-1134"},
                {"config": {
                    "field": 6,
                    "param": "a"
                }},
                {"channelname":"channel 2","channelnum":2,"diag":true,"time":0,"value":63.875,"warn":true},
                {"channelname":"first channel","channelnum":1,"diag":true,"time":0.5,"value":69.808,"warn":true},
                {"channelname":"Test Temperature Sensor","channelnum":7,"diag":true,"time":1,"value":72.599,"warn":true},
                {"channelname":"first channel","channelnum":1,"diag":true,"time":1.5,"value":75.04,"warn":true},
                {"channelname":"first channel","channelnum":1,"diag":true,"time":2,"value":76.011,"warn":true},
                {"channelname":"first channel","channelnum":1,"diag":true,"time":2.5,"value":75.933,"warn":true},
            ]
        );
        DataService.importDataset(
            [
                {"start": "2017-08-12-1135"},
                /*{"config": null/*{
                    "field": 8,
                    "param": "b"
                }},*/
                {"channelname":"first channel","channelnum":1,"diag":true,"time":3.5,"value":25.667,"warn":true},
                {"channelname":"Test Temperature Sensor","channelnum":7,"diag":true,"time":4,"value":0.198,"warn":true},
                {"channelname":"first channel","channelnum":1,"diag":true,"time":4.5,"value":-1.144,"warn":true},
                {"channelname":"first channel","channelnum":1,"diag":true,"time":5,"value":-1.673,"warn":true},
                {"channelname":"first channel","channelnum":1,"diag":true,"time":5.5,"value":-2.343,"warn":true},
            ]
        );
    };
    
    $scope.retrieveTest = function() {
        //console.log("retrieving");
        console.log(DataService.getDatasetList());
    };
    
    $scope.getObj = function() {
        console.log(DataService.getDatasetObj("2017-08-12-1135"));
    };
    
    $scope.rmData = function() {
        console.log(DataService.removeDataset("2017-08-12-1135"));
    };

    $scope.getConfig = function() {
        console.log(DataService.getConfig("2017-08-12-1135"));
    };

    $scope.setConfig = function() {
        DataService.setConfig("2017-08-12-1135", {
            field: 22222,
            param: "testing",
            extra: true
        });
    };
    
    $scope.getChannels = function() {
        console.log(DataService.getChannels("2017-08-12-1135"));
    };
    
    $scope.getChannelData = function() {
        console.log("Get channel data");
        console.log(DataService.getChannelData("2017-08-12-1135", 1));
    };

    $scope.getChannelDataTime = function() {
        console.log("Get channel data and time");
        console.log(DataService.getChannelDataTime("2017-08-12-1135", 1));
    };

    $scope.getChannelName = function() {
        console.log("Get channel 7 name");
        console.log(DataService.getChannelName("2017-08-12-1135", 7));
    };

    $scope.uploadDataset = function() {
        DataService.uploadDataset('http://cadelwatson.com:8666/', 'test', 'test', 'sqlite:///datasets', "2017-08-12-1135");
    };

    $scope.downloadDatasets = function() {
        DataService.downloadDatasets('http://cadelwatson.com:8666', 'test', 'test', 'sqlite:///datasets');
    };

    $scope.getChannelDiag = function() {
        console.log(DataService.getChannelDiag("2017-08-12-1135", 1));
    };

    $scope.loadOneDataset = function() {
        DataService.loadOneDataset("2017-08-12-1135");
    };

    $scope.loadDatasets = function() {
        DataService.loadDatasets();
    };
});