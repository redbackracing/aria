const angular = require('angular');

const Highcharts = require('highcharts');

window.Highcharts = Highcharts;
require('highcharts/modules/exporting')(Highcharts);

const app = angular.module('DashboardApp');

app.controller('WirelessController', ($scope, $websocket, $window, $interval) => {
    let dataStream = null;
    let promise;

    // Assign a default IP address and port number.
    let ipAddress = 'localhost';
    let portNumber = 8080;

    
    var recordingLocalLatency = false; // Change to default false
    var recordingTotalLatency = false;
    var times = [];
    
    $scope.initialOptions = {
        chart: {
            type: 'line',
        },
        legend: {
            enabled: false,
        },
        title: {
            text: null,
        },
        xAxis: {
            categories: [],
        },
        yAxis: {
        },
        series: [{
            data: [],
        }],
    };

    $scope.errorChartOptions = {
        chart: {
            type: 'pie',
        },
        legend: {
            enabled: false,
        },
        title: {
            text: null,
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: false,
                },
            },
        },

        series: [{
            name: 'Count',
            data: [{
                name: 'Nominal',
                color: '#7cb5ec',
            }, {
                name: 'Warning',
                color: '#e4d354',
            }, {
                name: 'Diagnostic',
                color: '#f45b5b',
            }],
        }],
    };

    $scope.channels = [];
    $scope.chartTypes = ['areaspline', 'column', 'line', 'pie', 'scatter'];
    
    // Controls button which toggles visibility of tools pane
    $scope.showTools = false;
    $scope.toggleTools = () => {
        $scope.showTools = !$scope.showTools;
    };
    
    // Controls button which toggles between real and mock data
    $scope.dataSource = 'Using telemetry';
    let useTelemetry = true;
    $scope.toggleSource = () => {
        if (useTelemetry) {
            useTelemetry = false;
            $scope.dataSource = 'Using mock data';
        } else {
            useTelemetry = true;
            $scope.dataSource = 'Using telemetry';
        }
    };

    // Controls button which allows latency to be logged
    $scope.latencyText = 'Latency logging off';
    var latencyMode = 0;
    $scope.switchLatencyMode = function() {
        latencyMode++;
        latencyMode %= 3;
        if (latencyMode == 0) {
            $scope.latencyText = 'Latency logging off';
            recordingLocalLatency = false;
            recordingTotalLatency = false;
        } else if (latencyMode == 1) {
            $scope.latencyText = 'Logging local latency';
            recordingLocalLatency = true;
        } else { // latencyMode == 2
            $scope.latencyText = 'Logging total latency';
            recordingLocalLatency = false;
            recordingTotalLatency = true;
        }
    };
    
    // Controls button which starts and stops streaming of data
    $scope.btnText = 'Start Streaming';
    $scope.toggle = function () {
        this.btnState = !this.btnState;
        if (this.btnState) {
            this.startStreaming();
            $scope.btnText = 'Stop Streaming';
        } else {
            this.destroyConnection();
            $scope.btnText = 'Start Streaming';
            if (recordingLocalLatency || recordingTotalLatency) {
                var totalTime = 0;
                for (var i = 0; i < times.length; i++) {
                    totalTime += times[i];
                }
                console.log("Average latency: " + totalTime/times.length + " ms");
            }
        }
    };

    $scope.startStreaming = () => {
        let data;

        // The created promise runs this function every second to request for data
        promise = $interval(() => {
            if (useTelemetry) {
                if (dataStream === null) {
                    $scope.createConnection();
                }
                if (dataStream !== null) {
                    dataStream.send(JSON.stringify({ request: '1' })); // Send request to server
                }
                dataStream.onMessage((message) => {
                    data = JSON.parse(message.data);
                    data = data.response.data;
                });
            } else {
                data = mockData();
            }

            if (recordingTotalLatency) {
                var startTime = data[0].sent; // Needs verification
            } else if (recordingLocalLatency) {
                var startTime = performance.now();
            }
            
            for (let i = 0; i < data.length; i += 1) {
                let j = 0;
                while (j < $scope.channels.length) {
                    if ($scope.channels[j].id === 'Channel' + data[i].channel) {
                        break;
                    }
                    j += 1;
                }
                if (j === $scope.channels.length) {
                    $scope.channels.push(new Channel('Channel' + data[i].channel, data[i].channelName));
                }
            }
            drawData(data);
            if (recordingLocalLatency || recordingTotalLatency) {
                var processingTime = (performance.now() - startTime);
                times.push(processingTime);
            }
        }, 1000);
    };

    function Point(color, y) {
        this.marker = { fillColor: color };
        this.color = color;
        this.y = y;
    }

    function Channel(id, name) {
        this.id = id;
        this.name = name;
        this.points = [];
        this.totalFlags = [0, 0, 0]; // Nominal, warning, diagnostic
    }

    const drawData = (data) => {
        for (let i = 0; i < $scope.channels.length; i += 1) {
            const graph = $('#' + $scope.channels[i].id).highcharts();
            const errorGraph = $('#' + $scope.channels[i].id + 'Error').highcharts();
            
            graph.yAxis[0].setTitle({
                text: data[i].unit
            });
            
            let highlight;
            if (data[i].diag === true) {
                highlight = '#f45b5b';
            } else if (data[i].warn === true) {
                highlight = '#e4d354';
            } else {
                highlight = '#7cb5ec';
            }

            let shiftPointsEnabled;
            if (graph.series[0].data.length > 30) {
                shiftPointsEnabled = true;
            } else {
                shiftPointsEnabled = false;
            }
            graph.series[0].addPoint({ marker: { fillColor: highlight }, y: data[i].value, color: highlight }, true, shiftPointsEnabled, false);
            $scope.channels[i].points.push(new Point(highlight, data[i].value));

            if (shiftPointsEnabled) {
                $scope.channels[i].points.shift();
            }

            if (data[i].diag) {
                $scope.channels[i].totalFlags[2] += 1;
            } else if (data[i].warn) {
                $scope.channels[i].totalFlags[1] += 1;
            } else {
                $scope.channels[i].totalFlags[0] += 1;
            }

            errorGraph.series[0].setData([{
                name: 'Nominal',
                color: '#7cb5ec',
                y: $scope.channels[i].totalFlags[0],
            }, {
                name: 'Warning',
                color: '#e4d354',
                y: $scope.channels[i].totalFlags[1],
            }, {
                name: 'Diagnostic',
                color: '#f45b5b',
                y: $scope.channels[i].totalFlags[2],
            }]);
        }
    };

    $scope.createConnection = () => {
        const ws = 'ws://';
        const url = ws.concat(ipAddress, ':', portNumber);
        dataStream = $websocket(url);
        dataStream.initialTimeout = 10;
    };

    $scope.destroyConnection = () => {
        $interval.cancel(promise);
        if (dataStream != null) {
            dataStream.close();
            dataStream = null;
        }
    };

    $scope.connectionDetails = function () {
        this.connectionState = !this.connectionState;
    };

    $scope.saveConnectionDetails = function () {
        if ($scope.connectionSettingsForm.$valid) {
            ipAddress = $scope.connectionSettings.ipAddress;
            portNumber = $scope.connectionSettings.portNumber;

            this.toggle();
            this.connectionDetails();
        }
    };

    $scope.changeChartType = (channelID, newType) => {
        const graph = $('#' + channelID).highcharts();
        graph.series[0].update({
            type: newType,
        }, false);

        let i = 0;
        while (i < $scope.channels.length) {
            if ($scope.channels[i].id === channelID) {
                break;
            }
            i += 1;
        }
        graph.series[0].setData(angular.copy($scope.channels[i].points));
        // Angular.copy is used since the data array is passed by reference to setData
        // Unusual or excessive data was being added when graph data was changed by Highcharts functions
        // Passing a new copy each time mitigates this issue
    };

    /* Used for testing when not connected to parallella */
    function mockData() {
        const mData = [];
        mData[0] = {};
        mData[0].channel = 1;
        mData[0].time = 0;
        mData[0].value = 5 * Math.random();
        mData[0].channelName = 'Temp 1';
        mData[0].diag = Math.random() >= 0.5;
        mData[0].warn = Math.random() >= 0.5;

        mData[1] = {};
        mData[1].channel = 2;
        mData[1].time = 0;
        mData[1].value = 5 * Math.random();
        mData[1].channelName = 'Pressure 6';
        mData[1].diag = Math.random() >= 0.5;
        mData[1].warn = Math.random() >= 0.5;

        mData[2] = {};
        mData[2].channel = 3;
        mData[2].time = 0;
        mData[2].value = 5 * Math.random();
        mData[2].channelName = 'Area 51';
        mData[2].diag = Math.random() >= 0.5;
        mData[2].warn = Math.random() >= 0.5;

        return mData;
    }
});
