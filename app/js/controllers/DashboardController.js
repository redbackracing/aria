const angular = require('angular');

const app = angular.module('DashboardApp');

app.controller('DashboardCtrl', ($scope, $state, DashboardService) => {
    DashboardService.dashboardList().then((response) => {
        const rawHTML = response.data;
        const availableDashboards = [];

        const dashboardNameRegex = /dashboards\/(.*).dash"/g;

        let match = dashboardNameRegex.exec(rawHTML);
        while (match != null) {
            availableDashboards.push(decodeURIComponent(match[1]));
            match = dashboardNameRegex.exec(rawHTML);
        }

        $scope.array = availableDashboards;
    });

    $scope.current = DashboardService.getCurrent();

    $scope.storeAndSwitch = (newDashboard) => {
        DashboardService.setCurrent(newDashboard);
        $scope.current = DashboardService.getCurrent();
        $state.go('Dashboard');
    };
});
