var angular = require('angular');
var ForerunnerDB = require('forerunnerdb');

var app = angular.module('DashboardApp');

app.service('DataService', function ($http, DashboardService) {
    var fdb = new ForerunnerDB();

    /* Get all avaliable dashboards (departments)
    */
    DashboardService.dashboardList().then((response) => {
        const rawHTML = response.data;
        const availableDashboards = [];

        const dashboardNameRegex = /dashboards\/(.*).dash"/g;

        let match = dashboardNameRegex.exec(rawHTML);
        while (match != null) {
            availableDashboards.push(decodeURIComponent(match[1]));
            match = dashboardNameRegex.exec(rawHTML);
        }

        const dashboards = availableDashboards;

        /* Loading persistent data stored in database
        *  Used when import/exporting dashboard layout
        */
        angular.forEach(dashboards, function(department) {
            fdb.db('ShadowBroker').collection(department).load();
            fdb.db('ShadowBroker').collection(department+'Data').load();
            fdb.db('ShadowBroker').collection(department+'Channels').load();
        })
    });

    /* Exports (saves) charts and data to the database
    *  These collections are persistent
    */
    this.saveDash = function (department, dash, selectedChannels) {
        fdb.db('ShadowBroker').collection(department).drop();
        fdb.db('ShadowBroker').collection(department).deferredCalls(false);
        fdb.db('ShadowBroker').collection(department).insert(dash);
        fdb.db('ShadowBroker').collection(department).save();

        fdb.db('ShadowBroker').collection(department+'Channels').drop();
        fdb.db('ShadowBroker').collection(department+'Channels').deferredCalls(false);
        fdb.db('ShadowBroker').collection(department+'Channels').insert(selectedChannels);
        fdb.db('ShadowBroker').collection(department+'Channels').save();
    };

    /* Copies the stored data (data stored when dashboard is exported) to the 'data' collection
    *  Returns the charts object
    *  Uses the format: [{name, display, id, position{top, height, left, width}, type}]
    *  Also returns the selectedChannel object
    *  Uses the format: [{id, label}]
    */
    this.loadDash = function (department) {
        const data = fdb.db('ShadowBroker').collection(department+'Data').find();
        fdb.db('ShadowBroker').collection('data').drop();
        fdb.db('ShadowBroker').collection('data').deferredCalls(false);
        fdb.db('ShadowBroker').collection('data').insert(data);
        const dash = fdb.db('ShadowBroker').collection(department).find();
        const channel = fdb.db('ShadowBroker').collection(department+'Channels').find();
        return {dash: dash, channel: channel};
    };

    // Inserts a dataset object into database
    this.importDataset = function(data) {
        if (this.getDatasetList().indexOf(data[0].start) != -1) { // If dataset does not already exist
            this.removeDataset(data[0].start);
        }
        var newSet = fdb.db('dataObjects').collection(data[0].start);
        newSet.deferredCalls(false);
        newSet.insert(data);
        newSet.save();
        this.saveDatasetList();
    };

    // Saves a list of datasets to separate database
    this.saveDatasetList = function() {
        fdb.db('datasetNames').collection('savedDatasets').deferredCalls(false);
        fdb.db('datasetNames').collection('savedDatasets').remove();
        fdb.db('datasetNames').collection('savedDatasets').insert(fdb.db('dataObjects').collections());
        fdb.db('datasetNames').collection('savedDatasets').save();
    };

    // Loads saved datasets
    this.loadDatasets = function() {
        fdb.db('datasetNames').collection('savedDatasets').load();
        var list = fdb.db('datasetNames').collection('savedDatasets').find();
        for (var i = 0; i < list.length; i++) {
            this.loadOneDataset(list[i].name);
        }
    };

    // Loads single dataset
    this.loadOneDataset = function(dataset) {
        fdb.db('dataObjects').collection(dataset).load();
    };

    // Returns array containing the names of the datasets in the database
    // Returns empty array if no datasets found
    this.getDatasetList = function() {
        var list = fdb.db('dataObjects').collections();
        var names = [];
        for (var i = 0; i < list.length; i++) {
            names.push(list[i].name)
        };
        return names;
    };

    // Returns entire dataset object, given a string containing the time in dataset
    // Returns empty array if no dataset found
    this.getDatasetObj = function(input) {
        return fdb.db('dataObjects').collection(input).find({}, {
            _id: 0 // Exclude ForerunnerDB-assigned ID field
        });
    };

    // Removes entire dataset, given a string containing the time in dataset
    this.removeDataset = function(input) {
        fdb.db('dataObjects').collection(input).drop();
        this.saveDatasetList();
    };

    // Returns the channel numbers and associated channel names seen in a dataset
    // Returned as an array with objects {channelnum: int, channelname: String}, sorted in ascending channel number order
    // Returns empty array if no dataset found
    this.getChannels = function(dataset) {
        var map = {};
        var array = [];

        var values = fdb.db('dataObjects').collection(dataset).find();

        for (var i = 0; i < values.length; i++) {
            if (values[i].hasOwnProperty('channelnum')) {
                var channel = values[i].channelnum;
                if (!map[channel]) {
                    array.push({channelnum: channel, channelname: values[i].channelname});
                    map[channel] = true;
                }
            }
        }

        array.sort(function(a, b) {return a.channelnum - b.channelnum});
        return array;
    };
    
    this.getChannelDataTime = function(dataset, channel) {
        var result = fdb.db('dataObjects').collection(dataset).find({
            channelnum: {
                $eq: channel
            }
        },
            {
                _id: 0,
                channelnum: 1,
                time: 1
            }
        );
        return result;
    };
    
    // Returns the value fields of all datapoints of a given channel in a dataset
    // Empty array returned if channel not found
    this.getChannelData = function(dataset, channel) {
        var result = fdb.db('dataObjects').collection(dataset).find({
            channelnum: {
                $eq: channel
            }
        },
            {
                $aggregate: "value"
            }
        );
        return result;
    };

    /* Returns the channel name for a given channel number
     * If channel number isn't found, null returned
     */
    this.getChannelName = function(dataset, channel) {
        var result = fdb.db('dataObjects').collection(dataset).find({
            channelnum: {
                $eq: channel
            }
        },
            {
                $aggregate: "channelname"
            }
        );

        if (result.length >= 1) {
            return result[0];
        } else {
            return null;
        };
    };

    // Returns the config file of the specified dataset
    // If dataset isn't found, null returned
    this.getConfig = function(dataset) {
        var result = fdb.db('dataObjects').collection(dataset).find({
            config: {
                $exists: true
            }
        });
        if (result.length == 1) {
            return result[0].config;
        } else {
            return null;
        };
    };

    /* Sets or overwrites the config file field of the specified dataset
       This function should not be used under normal circumstances as configs are
       recorded at start of run
    */
    this.setConfig = function(dataset, newConfig) {
        var modified = fdb.db('dataObjects').collection(dataset).update({
            config: {
                $exists: true
            }
        },{
            config: newConfig
        });

        if (modified.length == 0) {
            fdb.db('dataObjects').collection(dataset).insert({"config":newConfig});
        };
    };

    /* Returns the associated channel time for a given channel number
     * If channel number isn't found in database, empty array returned
     */
    this.getChannelTime = function(dataset, channel) {
        var result = fdb.db('dataObjects').collection(dataset).find({
            channelnum: {
                $eq: channel
            }
        },
            {
                $aggregate: "time"
            }
        );
        return result;
    };

    /* Returns the channel diagnostic for a given channel number
     * If channel number isn't found, empty array returned
     */
    this.getChannelDiag = function(dataset, channel) {
        var result = fdb.db('dataObjects').collection(dataset).find({
            channelnum: {
                $eq: channel
            }
        },
            {
                $aggregate: "diag"
            }
        );
        return result;
    };

    /* Returns the channel warning bools for a given channel number
     * If channel number isn't found, empty array returned
     */
    this.getChannelWarn = function(dataset, channel) {
        var result = fdb.db('dataObjects').collection(dataset).find({
            channelnum: {
                $eq: channel
            }
        },
            {
                $aggregate: "warn"
            }
        );
        return result;
    };

    // Downloads all datasets available from server and saves to database
    this.downloadDatasets = function(address, user, pwd, db) {
        var dataService = this;
        var list;
        $http({
            method: 'GET',
            url: dataService.appendToURL(address, 'info'),
            params: {username: user, password: pwd, database: db}
        }).then(function successCallback(response) {
            list = response.data.tests;
            for (var i = 0; i < list.length; i++) {
                dataService.downloadOneDataset(address, user, pwd, db, list[i]);
            }
        });
    };

    // Downloads specified dataset from server and saves to database
    this.downloadOneDataset = function(address, user, pwd, db, dataset) {
        var dataService = this;
        $http({
            method: 'GET',
            url: dataService.appendToURL(address, 'data'),
            params: {username: user, password: pwd, database: db, test: dataset}
        }).then(function successCallback(response) {
            dataService.importDataset(response.data.data);
        });
    };

    // Uploads a dataset which exists in the database to the server
    this.uploadDataset = function(address, user, pwd, db, dataset) {
        var dataService = this;
        var ds = fdb.db('dataObjects').collection(dataset).find({},{_id: 0});
        $http({
            method: 'POST',
            url: dataService.appendToURL(address, 'data'),
            data: {
                'data': ds,
                'username': user,
                'password': pwd,
                'database': db
            },
            headers: {'Content-Type': 'text/plain'}
        });
    };

    this.appendToURL = function(url, endpoint) {
        if (url.endsWith('/')) {
            return url + endpoint;
        } else {
            return url + '/' + endpoint;
        }
    };

    /* Returns the channel number corresponding to a particular channel name
     * Returns -1 if it cannot find the channel name
     * TODO: Improve query
     */
    this.convertNameToNumber = function(dataset, name) {
        var result = -1;
        var tmp;

        tmp = fdb.db('ShadowBroker').collection(dataset).find({
            channelname: {
                $eq: name
            }
        },
            {
                $aggregate: "channelnum"
            }
        );

        result = tmp[0];
        return result;
    };

    /* Specific function designed to output a channel data object
     * As preferred by HighCharts
     */
    this.getHighChartsChannelData = function(dataset, channelNum) {
        var result = {};

        result.channelNum = channelNum;

        result.channelName = this.getChannelName(dataset, channelNum);

        result.data = this.getChannelData(dataset, channelNum);
        result.time = this.getChannelTime(dataset, channelNum);

        var diag = this.getChannelDiag(dataset, channelNum);
        var warn = this.getChannelWarn(dataset, channelNum);
        result.diagLow = 1000;
        result.diagHigh = 0;
        result.warnLow = 1000;
        result.warnHigh = 0;

        result.color = new Array();

        for (var i = 0; i < result.time.length; i++) {
            result.time[i] = result.time[i].toFixed(2);
            if (diag[i] == true && result.data[i] < result.diagLow) {
                // This is a diagnostic value
                result.diagLow = result.data[i];
            }
            else if (diag[i] == true && result.data[i] > result.diagHigh) {
                // This is a diagnostic value
                result.diagHigh = result.data[i];
            }
            else if (warn[i] == true && result.data[i] < result.warnLow) {
                // This is a warning value
                result.warnLow = result.data[i];
            }
            else if (warn[i] == true && result.data[i] > result.warnHigh) {
                // This is a warning value
                result.warnHigh = result.data[i];
            }

        }

        if (Math.abs(result.diagLow - result.warnLow) < 0.01) {
            result.diagLow = 1000;
        }
        if (Math.abs(result.diagHigh - result.warnHigh) < 0.01) {
            result.diagHigh = 0;
        }
        return result;
    };
});
