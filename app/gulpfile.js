const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const browserify = require('browserify');
const babelify = require('babelify');
const ngAnnotate = require('browserify-ngannotate');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const gulpUtils = require('gulp-util');
const ngHtml2Js = require('gulp-ng-html2js');
const concat = require('gulp-concat');
const eslint = require('gulp-eslint');
const jasmine = require('gulp-jasmine-browser');
const replace = require('gulp-replace');

gulp.task('lint', () => {
    return gulp.src(['**/*.js', '!node_modules/**', '!scripts/**', '!specs/**', '!dist/**'])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('build-css', () => {
    return gulp.src('./css/*')
        .pipe(sourcemaps.init())
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('./dist'));
});

gulp.task('build-js', () => {
    const b = browserify({
        entries: './js/DashboardApp.js',
        debug: true,
        paths: ['./js/controllers', './js/services', './js/directives'],
        transform: [ngAnnotate],
    });

    return b.transform(babelify)
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
        .on('error', gulpUtils.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist/js'));
});

gulp.task('build-js-tests', () => {
    const b = browserify({
        entries: './js/DashboardApp.js',
        debug: true,
        paths: ['./js/controllers', './js/services', './js/directives'],
        transform: [ngAnnotate],
    });

    return b.transform(babelify)
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
        .on('error', gulpUtils.log)
        .pipe(sourcemaps.write('./'))
        .pipe(replace('ariaDirectives', 'ariaDirectives, ngMock'))
        .pipe(gulp.dest('./dist/js'));
});

gulp.task('build-template-cache', () => {
    return gulp.src('./partials/*.html')
        .pipe(ngHtml2Js({
            moduleName: 'ariaPartials',
            prefix: '/partials/',
        }))
        .pipe(concat('templateCachePartials.js'))
        .pipe(gulp.dest('./dist'));
});

gulp.task('build-directive-cache', () => {
    return gulp.src('./directives/*.html')
        .pipe(ngHtml2Js({
            moduleName: 'ariaDirectives',
            prefix: '/directives/',
        }))
        .pipe(concat('templateCacheDirectives.js'))
        .pipe(gulp.dest('./dist'));
});

gulp.task('build-dashboards', () => {
    return gulp.src('./dashboards/*')
        .pipe(gulp.dest('./dist/dashboards'));
});

gulp.task('build-resources', () => {
    return gulp.src('./resources/*')
        .pipe(gulp.dest('./dist/resources'));
});

gulp.task('build', ['build-css', 'build-dashboards', 'build-template-cache', 'build-directive-cache', 'build-js', 'build-resources'], () => {
    return gulp.src('index.html')
        .pipe(gulp.dest('dist'));
});

gulp.task('build-tests', ['build-css', 'build-dashboards', 'build-template-cache', 'build-directive-cache', 'build-js-tests'], () => {
    return gulp.src('index.html')
        .pipe(gulp.dest('dist'));
});

gulp.task('test', ['build-tests'], () => {
    return gulp.src(['dist/js/**/*.js', 'specs/**/*.js'])
        .pipe(jasmine.specRunner())
        .pipe(jasmine.server({ port: 8000 }));
});

gulp.task('dist', ['build'], () => {
    return gulp.src('distscripts/*')
        .pipe(gulp.dest('dist'));
});